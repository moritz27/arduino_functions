#include "time_interface.h"

void initTime(const char* ntp_server){
    configTime(0, 0 , ntp_server);
}

unsigned long getTime() {
  time_t now;
  struct tm timeinfo;
  if (!getLocalTime(&timeinfo, 1000)) {
    //Serial.println("Failed to obtain time");
    return(0);
  }
  time(&now);
  return now;
}