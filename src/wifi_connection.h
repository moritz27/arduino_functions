#include <Arduino.h>
#include "debug_interface.h"
#include "helper.h"

#if defined(ESP8266)
#include <ESP8266WifiMulti.h>
void setup_wifi_multi(ESP8266WiFiMulti wifiMulti, const char* wifiSsids[], const char* wifiPasses[], const uint8_t wifiNum, const uint8_t retries = 10);
#elif defined(ESP32)
#include <WiFiMulti.h>
void setup_wifi_multi(WiFiMulti wifiMulti, const char* wifiSsids[], const char* wifiPasses[], const uint8_t wifiNum, const uint8_t retries = 10);
#else
#error "This ain't a ESP8266 or ESP32, no Wifi!"
#endif
