#include <PubSubClient.h>
#include <ArduinoJson.h>
#include "debug_interface.h"

void setup_mqtt(PubSubClient &mqttClient, const char *mqttServer, const uint16_t mqttPort = 1883, const char *hostname = "ESP2MQTT", const uint8_t retries = 5);
uint32_t send_mqtt_string(PubSubClient &mqttClient, const char *hostname, const char *content, const char *topic, const uint8_t retries = 5);
uint32_t send_mqtt_json(PubSubClient &mqttClient, const char *hostname, DynamicJsonDocument &json, const char *topic, const uint8_t retries = 5);
