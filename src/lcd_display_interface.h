#include <Adafruit_GFX.h>
#include <Adafruit_ST7789.h>
#include <SPI.h>

void setupDisplay(const char *header);
void clearText();
void reprintText(const char *newText, const uint16_t color = ST77XX_WHITE);
void flashText(const char *newText, const uint16_t color = ST77XX_WHITE, const uint8_t blankTime = 50);