#include "secrets.h"

/* ########################## BLE DEVICE ########################## */
// Chose which BLE device should be used
#define BLE_DEVICE_B35T
// #define BLE_DEVICE_ALLPOWERS

/* ########################## NAMES ########################## */
#if !defined(MY_HOSTNAME)
#define MY_HOSTNAME "ESP32_BLE_Gateway"
#endif
#if defined(BLE_DEVICE_B35T)
#define MEASUREMENT_NAME "Owon_B35T"
#elif defined(BLE_DEVICE_ALLPOWERS)
#define MEASUREMENT_NAME "AP_S300"
#endif

/* ########################## OTA ########################### */
// #define OTA

/* ########################## WIFI ########################## */
#define USE_WIFI
// #define RESTART_IF_NO_WIFI

/* ########################## MQTT ########################## */
// #define MQTT
#if defined(MQTT)
#define TOPIC MY_HOSTNAME MEASUREMENT_NAME
#if !defined(MQTT_SERVER)
#define MQTT_SERVER "YourMqttServerIP"
#endif
#endif

/* ########################## INFLUXDB ########################## */
#define INFLUXDB
#if defined(INFLUXDB)
#if !defined(INFLUXDB_URL)
#define INFLUXDB_URL "YourInfluxDbServerIp"
#endif
#if !defined(INFLUXDB_TOKEN)
#define INFLUXDB_TOKEN "YourInfluxDbToken"
#endif
#if !defined(INFLUXDB_ORG)
#define INFLUXDB_ORG "moco"
#endif
#if !defined(INFLUXDB_BUCKET)
#define INFLUXDB_BUCKET MY_HOSTNAME
#endif
#endif

/* ########################## LCD DISPLAY ########################## */
#if defined(TFT_CS) && defined(TFT_DC) && defined(TFT_RST)
#define LCD_DISPLAY
#endif

/* ########################## SD CARD ########################## */
#if defined(SD_SCK) && defined(SD_MISO) && defined(SD_MOSI) && defined(SD_CS)
// #define SD_CARD
#endif

/* ########################## NTP Time ########################## */
//default is "pool.ntp.org" set other one if needed
// #define NTP_SERVER "pool.ntp.org"