#include <InfluxDbClient.h>
#include <ArduinoJson.h>
#include "debug_interface.h"

bool writeJsonToInflux(InfluxDBClient &influxClient, const char *meas_name, DynamicJsonDocument &json);