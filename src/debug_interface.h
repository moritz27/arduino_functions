/* ########################## DEBUG OUTPUT ########################## */
// Chose which debug output should be printed to Serial
#define DEBUG_OUTPUT
#define DEBUG_WIFI_OUTPUT
#define DEBUG_OTA_OUTPUT
#define DEBUG_LED_OUTPUT
// #define DEBUG_MQTT_OUTPUT
#define DEBUG_BLE_INTERFACE_OUTPUT
// #define DEBUG_BLE_DEVICES_OUTPUT
// #define DEBUG_INFLUXDB_OUTPUT
// #define DEBUG_DISPLAY_OUTPUT
#define DEBUG_SDCARD_OUTPUT

/* ########################## DEBUG ########################## */
#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)     \
  ((byte)&0x80 ? '1' : '0'),     \
      ((byte)&0x40 ? '1' : '0'), \
      ((byte)&0x20 ? '1' : '0'), \
      ((byte)&0x10 ? '1' : '0'), \
      ((byte)&0x08 ? '1' : '0'), \
      ((byte)&0x04 ? '1' : '0'), \
      ((byte)&0x02 ? '1' : '0'), \
      ((byte)&0x01 ? '1' : '0')

#if defined(DEBUG_OUTPUT)
#define DEBUG_MSG(...) Serial.printf(__VA_ARGS__)
#else
#define DEBUG_MSG(...)
#endif

#if defined(DEBUG_LED_OUTPUT)
#define DEBUG_MSG_LED(...) Serial.printf(__VA_ARGS__)
#else
#define DEBUG_MSG_LED(...)
#endif

#if defined(DEBUG_WIFI_OUTPUT)
#define DEBUG_MSG_WIFI(...) Serial.printf(__VA_ARGS__)
#else
#define DEBUG_MSG_WIFI(...)
#endif

#if defined(DEBUG_OTA_OUTPUT)
#define DEBUG_MSG_OTA(...) Serial.printf(__VA_ARGS__)
#else
#define DEBUG_MSG_OTA(...)
#endif

#if defined(DEBUG_MQTT_OUTPUT)
#define DEBUG_MSG_MQTT(...) Serial.printf(__VA_ARGS__)
#else
#define DEBUG_MSG_MQTT(...)
#endif

#if defined(DEBUG_BLE_INTERFACE_OUTPUT)
#define DEBUG_MSG_BLE_INTERFACE(...) Serial.printf(__VA_ARGS__)
#else
#define DEBUG_MSG_BLE_INTERFACE(...)
#endif

#if defined(DEBUG_BLE_DEVICES_OUTPUT)
#define DEBUG_MSG_BLE_DEVICES(...) Serial.printf(__VA_ARGS__)
#else
#define DEBUG_MSG_BLE_DEVICES(...)
#endif

#if defined(DEBUG_INFLUXDB_OUTPUT)
#define DEBUG_MSG_INFLUXDB(...) Serial.printf(__VA_ARGS__)
#else
#define DEBUG_MSG_INFLUXDB(...)
#endif

#if defined(DEBUG_DISPLAY_OUTPUT)
#define DEBUG_MSG_DISPLAY(...) Serial.printf(__VA_ARGS__)
#else
#define DEBUG_MSG_DISPLAY(...)
#endif

#if defined(DEBUG_SDCARD_OUTPUT)
#define DEBUG_MSG_SDCARD(...) Serial.printf(__VA_ARGS__)
#else
#define DEBUG_MSG_SDCARD(...)
#endif