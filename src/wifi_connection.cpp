#include "wifi_connection.h"

uint8_t doWifiScan()
{
  DEBUG_MSG_WIFI("Scanning WIFI.. ");
  int16_t n = WiFi.scanNetworks();
  DEBUG_MSG_WIFI(" done! \n");
  if (n == 0)
  {
    DEBUG_MSG_WIFI("No Networks found!");
  }
  else
  {
    DEBUG_MSG_WIFI("%d Networks found \n", n);
    for (int i = 0; i < n; ++i)
    {
      DEBUG_MSG_WIFI("  %d: %s (%d)\n", i + 1, WiFi.SSID(i).c_str(), WiFi.RSSI(i));
      delay(10);
    }
  }
  return n;
}

#if defined(ESP8266)
void setup_wifi_multi(ESP8266WiFiMulti wifiMulti, const char* wifiSsids[], const char* wifiPasses[], const uint8_t retries)
#elif defined(ESP32)
#include <WiFiMulti.h>
void setup_wifi_multi(WiFiMulti wifiMulti, const char* wifiSsids[], const char* wifiPasses[], const uint8_t wifiNum, uint8_t retries)
#endif
{
  WiFi.mode(WIFI_STA);

  doWifiScan();

  DEBUG_MSG_WIFI("Trying to connect to WIFI..\n");

  DEBUG_MSG_WIFI("Number of Wifis: %d \n", wifiNum);

  for(int i=0; i<wifiNum; i++){
    DEBUG_MSG_WIFI("%s %s \n", wifiSsids[i], wifiPasses[i]);
    wifiMulti.addAP(wifiSsids[i], wifiPasses[i]);
  }
  if (wifiMulti.run(10000) == WL_CONNECTED)
  {
    IPAddress ip = WiFi.localIP();
    DEBUG_MSG_WIFI("WiFi connected to %s (%d), IP address: %d.%d.%d.%d \n", WiFi.SSID().c_str(), WiFi.RSSI(), ip[0], ip[1], ip[2], ip[3]);
  }
  else
  {
    DEBUG_MSG_WIFI("WIFI could not be connected");

#if defined(RESTART_IF_NO_WIFI)
    DEBUG_MSG_WIFI(" restarting ESP now..\n");
    delay(100);
    ESP.restart();
    delay(100);
#endif
    DEBUG_MSG_WIFI("\n");
  }
}